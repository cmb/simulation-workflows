# Notes for developers

## Python scripts
For compatibility with pybind11 bindings
* Import smtk submodules where needed
* Note that smtk.simulation is needed in files where
  ExportCMB() function is called.
* Must explicitly pass integer to smtk.modelManager.entitiesMatchingFlags()
* Pybind doesn't need or have smtk.to_concrete()
* Pybind doesn't need or have smtk.attribute.System.CastTo()
* Pybind doesn't need or have smtk.attribute.AttributePtr
* Pybind uses unicode instead of str, so use isinstance(object, basestring)

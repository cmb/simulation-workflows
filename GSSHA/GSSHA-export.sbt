<SMTK_AttributeSystem Version="2">
  <Definitions>
    <AttDef Type="ExportSpec" Lavel="Simulation" BaseType="" Version="0">
      <ItemDefinitions>
        <String Name="AnalysisTypes" Label="Analysis Types" Version="0" AdvanceLevel="99" NumberOfRequiredValues="1" Extensible="true" />
        <File Name="PythonScript" Label="Python script" Version="0" AdvanceLevel="0" NumberOfRequiredValues="1"
              ShouldExist="true" FileFilters="Python files (*.py);;All files (*.*)">
          <DefaultValue>GSSHA.py</DefaultValue>
        </File>
        <String Name="ProjectName" Label="Project Name" Version="0">
          <BriefDescription>Name (prefix) to use for all GSSHA files</BriefDescription>
        </String>
        <Directory Name="ProjectPath" Label="Project Path"
                   Version="0" NumberOfRequiredValues="1" ShouldExist="false">
          <BriefDescription>Filesystem folder for all GSSHA files</BriefDescription>
        </Directory>
      </ItemDefinitions>
     </AttDef>
   </Definitions>
  <Views>
    <View Type="Group" Title="Export" TopLevel="true">
      <Views>
        <View Title="ExportSpec" />
      </Views>
    </View>
    <View Type="Instanced" Title="ExportSpec">
      <InstancedAttributes>
        <Att Name="Options" Type="ExportSpec" />
      </InstancedAttributes>
    </View>
  </Views>

</SMTK_AttributeSystem>

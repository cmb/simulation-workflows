# OpenFOAM MotorBike Example

CMB ModelBuilder can be used to reproduce the OpenFOAM tutorial example.
Following are some instructions to walk through the process. The reader
should have some familiarization with ModelBuilder before undertaking
this.


## Caveats

At this point, CMB's OpenFOAM functionality is limited, including the following.

* The current software is hard-coded to run OpenFOAM executables in a
  single process.
* The current software is hard-coded to work with a single
  object (the "obstacle") inside a wind tunnel. For demonstration, the
  default obstacle is the motorbike.obj file that is included in the
  OpenFOAM tutorial. There is a copy of the motorbike.obj file in the
  SMTK source repository. There is also a small wedge.stl file in this
  folder, which can be used instead of the motorbike.obj file.
* The current software is setup to only run the simplefoam simulator.


## Setup
To get started, there are a number of one-time setup steps:

### Install or build CMB ModelBuilder
Linux: on linux systems, you typically run the ModelBuilder application
from the command line.

macOS: you should be able to open the ModelBuilder.app from the desktop
or the command line.

### Install OpenFOAM
Linux: on linux systems, install OpenFOAM using the instructions
on the OpenFOAM site. (Docker image on linux is not currently supported.)

macOS: On macOS, the Docker image is the supported install method.

### Set OpenFOAM environment
Linux: On linux systems,
before running either ModelBuilder or OpenFOAM executables, first source
the OpenFOAM environment. Because of potential conflicts between
ParaView versions, it is strongly recommended to NOT use the default
bashrc file installed with OpenFOAM. Instead, use the modifed bashrc file
in this folder, e.g.

  ```source .../simulation-workflows/OpenFOAM/internal/motorbike/bashrc```

The only change we made to the file is to comment out the line configuring
ParaView.

macOS: On macOS systems, start the OpenFOAM Docker image.

### Set ModelBuilder to Session Centric Modeling
You must also configure ModelBuilder to use "Session Centric Modeling" in
order to run the OpenFOAM demo.

Linux: on linux systems, use the "Edit" --> "Settings" menu to open the
Applications Settings window.

macOS: on macOS systems, use the "ModelBuilder" --> "Preferences..."
menu item to open the Application Settings window.

Then, in the Application Settings window: in the left-hand panel, open the
"App Specific" category and select the "ModelBuilder" item.
In the right-hand panel, check (enable) the "Session-Centric Modeling" option.


## Generate the Model and Mesh

Make sure you completed all of the setup steps listed above.
After starting ModelBuilder, initialize an OpenFOAM "session" by using this menu item:

    File -> New Session... -> openfoam

Following that, execute the following 5 "operators" in sequence.

### 1. Session - Set Working Directory
Open the operator context menu by right-clicking on the session in the "Model" tab,
and then selecting "Session - Set Working Directory". An operator panel will pop
up, and in that panel, enter a "Working Directory". This is the directory where all
of the OpenFOAM files will be written throughout the session.

Point to an empty directory (creating one as needed), and click the "Apply" button.

### 2. Model - Set Main Controls
At the top of the operator panel is a selection box, where you can navigate to the
other operators as needed.

Select the "Model - Set Main Controls" operator. You can use the default values
in this panel, and click the "Apply" button at the bottom.

### 3. Model - Create Wind Tunnel
Select the "Model - Create Wind Tunnel" operator. You can again edit the parameters
here, but the default values are set up to run the motorbike example.

When you click the "Apply" button, a six-sided volume is added to the session, and
can be seen in the 3D view.

If you do not see a model added to the 3D view, and/or there is a
console message that the operation failed, this indicates that
ModelBuilder was unable to find the OpenFOAM packages/executables.
Make sure the OpenFOAM is installed and available in the system path.

### 4. Model - Add Auxilliary Geometry
Select this operator and add the file name/path to the motorbike.obj
file, or an alternative geometry file if you prefer.
As noted above, you can get the motorbike.obj file from either the
OpenFOAM tutorial, or there is also a copy under the smtk data
directory.

After clicking the "Apply" button, the motorbike geometry is added to the model
and is visible in the 3D display. (You might need to hide one or more sides of the
wind tunnel to see it.)

Also note that the options in this operator for "scale", "rotate",
and "translate" are not implemented; leave these at their default values.

### 5. Model - Add Obstacle
This panel is configured for the motorbike. If using different obstacle
geometry, you might need to adjust the refinement box settings to the
size and/or position of the obstacle.

Once you click the "Apply" button,
ModelBuilder will run a number of OpenFOAM executables to generate the model
and mesh for analysis. This process might take several minutes on a desktop machine.
Check the console to determine when the processing is complete. The last log
message is

    INFO: Model - Add Obstacle: operation succeeded


Once that processing is complete, you can close the operator panel. You can also
check the session directory you entered in step 1 and see what files have been
generated.


## Generate the Simulation Attributes

The next step is to load the simulation template file. Use the "File --> Load Simulation Template"
menu item, and navigate to the OpenFOAM directory and select the [OpenFOAM.crf](../../OpenFOAM.crf) 
file. A new "Attribute" tab will be added to the UI.

You can use the default values in the "Analysis" and "Materials" tabs.

### Boundary Conditions

Click on the "Boundary Conditions" tab and add 6 boundary condition
attributes, as listed below. (If you are using a different obstacle
than the motorbike, replace "motorBike" the name of your obstacle.)


    New (bc0)---> back, ceiling, front
    Variable --> Slip

    New (bc1) --> motorBike
    Variable ---> Velocity
    Values -----> 0, 0, 0

    New (bc2) --> floor, inlet
    Variable ---> Velocity
    Values -----> 20, 0, 0

    New (bc3) --> outlet
    Variable ---> Inlet Outlet
    Values -----> 20, 0, 0

    New (bc4) --> floor, inlet, motorBike
    Variable ---> Zero Gradient

    New (bc5) --> outlet
    Variable ---> Pressure
    Value ------> 0


### Turbulence Model

In the Turbulence Model tab:

1. Set the "Turbulence Model Class" to "RAS"
2. Set the "RAS Model" to kOmegaSST.


## Generate the OpenFOAM input deck
To write the OpenFOAM simulation input files, use the
"File --> Export Simulation Files" menu item.
The default settings in the popup window should be appropriate, so click OK.

At this point, ModelBuilder should have generated the input files
needed to run OpenFOAM.


## Run the simulation

To run the OpenFOAM simulator:

1. Open a new terminal and navigate to the working directory.
2. For linux systems, once again source the bashrc file that is in the
    OpenFOAM/internal/motorBike directory.
3. Run the "Allrun" script.

The Allrun script runs three OpenFOAM executables in order:

1. patchSummary 
2. potentialFoam
3. simpleFoam

The first two should run in a few seconds.
The simpleFoam executable will take many minutes on a desktop machine.
Using the default solver settings, the executable will run 500 steps,
writing the results every 100 steps.


## Visualize the results

You can view the OpenFOAM output in the parafoam executable, if you use
a separate terminal and source the OpenFOAM bashrc file.

Alternatively, you can also use the ParaView executable that is
distributed with ModelBuilder. In ParaView, open the .foam file that
was generated by ModelBuilder.

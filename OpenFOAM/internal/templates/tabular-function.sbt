<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <Definitions>
    <AttDef Type="sim-expression" Abstract="1" Association="None"/>
    <AttDef Type="tabular-function" Label="Tabular Function" BaseType="sim-expression" Version="0">
      <ItemDefinitions>
        <Group Name="ValuePairs" Label="Value Pairs" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="X" Version="0" AdvanceLevel="0" NumberOfRequiredValues="0" Extensible="true"/>
            <Double Name="Value" Version="0" AdvanceLevel="0" NumberOfRequiredValues="0" Extensible="true"/>
          </ItemDefinitions>
        </Group>
        <String Name="Sim1DTable" Version="0" AdvanceLevel="0" NumberOfRequiredValues="1"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeSystem>

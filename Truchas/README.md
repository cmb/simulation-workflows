# Truchas

The Truchas workflow files have been moved to the Truchas plugin repository at
https://gitlab.kitware.com/cmb/plugins/truchas-extensions .

Note that the Truchas "material" and "induction heating" attributes use custom
views built as part of the Truchas plugin.
